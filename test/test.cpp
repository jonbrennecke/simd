#include "../src/basic_simd.h"
#include "../src/array.h"
#include <assert.h>
#include <math.h>
#include <iostream>

void test_basic_simd() {
	simd::basic_simd<double, 4> a({ 2, 3, 0, 0});

	auto b = simd::add(a, a);
	auto sq = simd::sqrt(b);

	auto c = a + a;
	assert(c[0] == b[0]);
	assert(c[1] == b[1]);

	auto mul = simd::mul(a, a);

	assert(mul[0] == 4);
	assert(b[0] == 4);
	assert(b[1] == 6);
	assert(sq[0] == 2);
	assert(sq[1] == sqrt(6));
}

void test_array() {
	simd::array<double, 7> arr(std::make_tuple(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0));

	assert(arr[0] == 1.0);
	assert(arr[3] == 4.0);
	assert(arr[6] == 7.0);
}

int main()
{
	test_basic_simd();
	test_array();
	return 0;
}
