#pragma once

#include <array>
#include <utility>
#include <initializer_list>
#include <stdlib.h>
#include <type_traits>

#include "basic_simd.h"
#include "utils/applyn.h"
#include "utils/make_integer_range.h"
#include "utils/compare.h"
#include "utils/apply_constant.h"

namespace simd {

    namespace impl {

        template<typename T>
        constexpr inline std::size_t get_simd_size() {
            return 256 / (sizeof(T) * 8);
        }

        template<typename T, std::size_t S>
        constexpr inline std::size_t get_array_capacity() {
            const std::size_t n = get_simd_size<T>();
            return ((n - S % n) + S) / n;
        }

    }

    template<typename T, std::size_t S>
    struct array {

        constexpr static std::size_t size = S;
        constexpr static std::size_t simd_size = impl::get_simd_size<T>();
        constexpr static std::size_t data_array_size = impl::get_array_capacity<T, S>();

        typedef basic_simd<T, simd_size> simd_type;
        typedef array<T, S> self_type;
        typedef std::array<simd_type, data_array_size> value_type;


        constexpr array(std::initializer_list<T> l) 
            : data(flatmap_tuple(l)) 
            {}

        template <typename ... Args>
        constexpr array(std::tuple<Args...> && t) 
            : data(flatmap_tuple(std::forward<decltype(t)>(t)))
            {}

        constexpr array(std::array<T, S> && a) 
            : data(flatmap_tuple(std::forward<decltype(a)>(a)))
            {}

        template <typename ... Args>
        constexpr inline value_type flatmap_tuple(std::tuple<Args...> && a) {
            return __flatmap_tuple(std::forward<decltype(a)>(a), std::make_index_sequence<data_array_size>());
        }

        template <typename A, std::size_t ... I>
        constexpr inline value_type __flatmap_tuple(A && a, std::index_sequence<I...>) 
        {
            return {{ get_sub_range<I * simd_size>(std::forward<decltype(a)>(a)) ... }};
        }

        template<std::size_t Begin, typename A>
        constexpr inline decltype(auto) get_sub_range(A && a) {
            return __get_sub_range<decltype(a), Begin>(std::forward<decltype(a)>(a), utils::make_index_range<Begin, Begin + simd_size>());
        }

        template<typename A, std::size_t Begin, std::size_t ... I, typename = void>
        constexpr inline simd_type __get_sub_range(A && a, std::index_sequence<I...>) {
            typedef std::array<double, simd_size> array_type;
            array_type zeros = utils::apply_constant<double, simd_size, array_type>(0);
            auto t = std::tuple_cat(a, zeros);
            return simd_type(std::get<I>(t) ...);
        }

        template<typename A, std::size_t Begin, std::size_t ... I, typename std::enable_if<utils::is_less_than<Begin + simd_size, S>::value>::type>
        constexpr inline simd_type __get_sub_range(A && a, std::index_sequence<I...>) {
            return simd_type(std::get<I>(a) ...);
        }

        constexpr inline const T operator[](const size_t index) const {
            auto res = div(index, simd_size);
            return data[res.quot][res.rem];
        }
        
        private:
            std::array<simd_type, data_array_size> data;

    } ALIGNED(32);

}
