#pragma once

#include "types.h"
#include "set.h"

namespace simd {

    template<typename, std::size_t, typename = void>
    struct basic_simd; /* undefined */

    template<typename T, std::size_t S>
    struct basic_simd<T, S, typename std::enable_if<test_simd_size<T, S>::value>::type>
    {
    public:    
        typedef simd_t_for<T, S> value_type;
        typedef basic_simd<T, S> self_type;
    
        template<typename ... Args>
        constexpr basic_simd(Args && ... args);
        constexpr basic_simd(value_type _value);

        constexpr inline const T operator[](const size_t index) const;
        constexpr inline const value_type & operator*() const;

        constexpr inline self_type operator+(const self_type & other) const;
        constexpr inline self_type operator*(const self_type & other) const;
    
    private:
        ALIGNED(32) value_type value;
    };

}

#include "math/add.h"
#include "math/mul.h"
#include "math/sqrt.h"

namespace simd {
        
        template<typename T, std::size_t S>
        using self_type = basic_simd<T, S, typename std::enable_if<test_simd_size<T, S>::value>::type>;
    
        template<typename T, std::size_t S>
        template<typename ... Args>
        constexpr self_type<T, S>::basic_simd(Args && ... args) : value(set<T, S>(std::forward<Args>(args)...))
        {}
        
        template<typename T, std::size_t S>
        constexpr self_type<T, S>::basic_simd(value_type _value) : value(_value)
        {}

        template<typename T, std::size_t S>
        constexpr inline const T self_type<T, S>::operator[](const size_t index) const {
            return value[index];
        }

        template<typename T, std::size_t S>
        constexpr inline const typename self_type<T, S>::value_type & self_type<T, S>::operator*() const {
            return value;
        }

        template<typename T, std::size_t S>
        constexpr inline typename self_type<T, S>::self_type self_type<T, S>::operator+(const self_type & other) const {
           return add(*this, other);
        }

        template<typename T, std::size_t S>
        constexpr inline typename self_type<T, S>::self_type self_type<T, S>::operator*(const self_type & other) const {
           return mul(*this, other);
        }

}