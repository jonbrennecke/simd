#pragma once

#include "../basic_simd.h"

namespace simd {

    namespace impl {
        
    	template<typename T, std::size_t S>
    	using value_type = typename basic_simd<T, S>::value_type;

        template<typename T, std::size_t S>
        constexpr inline decltype(auto) add_impl(const value_type<T, S> &, const value_type<T, S> &);

        template <>
        inline decltype(auto) add_impl<double, 4>(const value_type<double, 4> & a, const value_type<double, 4> & b)
        {
            return _mm256_add_pd(a, b);
        }
    }

    template<typename T, std::size_t S>
    constexpr decltype(auto) add(const basic_simd<T, S> & a, const basic_simd<T, S> & b) {
    	return basic_simd<T, S>(impl::add_impl<T, S>(*a, *b));
    }
}