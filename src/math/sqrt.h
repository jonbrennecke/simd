#pragma once

#include "../basic_simd.h"

namespace simd {

    namespace impl {
        
    	template<typename T, std::size_t S>
    	using value_type = typename basic_simd<T, S>::value_type;

        template<typename T, std::size_t S>
        constexpr inline decltype(auto) sqrt_impl(const value_type<T, S> &);

        template <>
        inline decltype(auto) sqrt_impl<double, 4>(const value_type<double, 4> & a)
        {
            return _mm256_sqrt_pd(a);
        }
    }

    template<typename T, std::size_t S>
    constexpr decltype(auto) sqrt(basic_simd<T, S> & a) {
    	return basic_simd<T, S>(impl::sqrt_impl<T, S>(*a));
    }
}