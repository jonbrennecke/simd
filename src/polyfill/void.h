#pragma once

/**
 *
 * Polyfill implementation of `void_t` from C++17
 *
 */


template<class...>
struct void_ {
	using type = void;
};

template<class... T>
using void_t = typename void_<T...>::type;
