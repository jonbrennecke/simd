#pragma once

#include "types.h"
#include "noop.h"
#include <type_traits>
#include <random>

namespace simd {

    template <typename, typename = void>
    struct rand_traits; /* undefined */

    template <typename T>
    struct rand_traits<T, typename std::enable_if<std::is_integral<T>::value>::type> {
        typedef std::uniform_int_distribution<T> distribution_type;
    };

    template <typename T>
    struct rand_traits<T, typename std::enable_if<std::is_floating_point<T>::value>::type> {
        typedef std::uniform_real_distribution<T> distribution_type;
    };

    namespace impl {

        template <typename Generator, typename Distribution, std::size_t ... I>
        constexpr inline decltype(auto) set_rand_impl(Generator & gen, Distribution & dist, std::index_sequence<I...>) {
            return simd::set<typename Distribution::result_type, sizeof...(I)>(util::noop<I>(dist(gen))...);
        }

    }

    template<
        typename T,
        std::size_t S,
        typename Distribution = typename rand_traits<T>::distribution_type,
        typename Generator = std::mt19937
        >
    decltype(auto) set_rand(const std::size_t upper, const std::size_t lower) {
        std::random_device rd;
        Generator gen(rd());
        Distribution dist(lower, upper);
        return impl::set_rand_impl(gen, dist, std::make_index_sequence<S>{});
    }

}
