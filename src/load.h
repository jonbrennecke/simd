#pragma once

template<typename T, std::size_t S>
decltype(auto) load(const T * a);

template<>
decltype(auto) load<double, 2>(const double * a) {
    return _mm_load_pd(std::forward<decltype(a)>(a));
}

template<>
decltype(auto) load<float, 4>(const float * a) {
    return _mm_load_ps(std::forward<decltype(a)>(a));
}
