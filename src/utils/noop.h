#pragma once

#include <utility>

namespace simd {

	namespace utils {

		template<std::size_t I, typename T>
		constexpr inline decltype(auto) noop(T && t) {
		    return std::forward<T>(t);
		}

	}
	
}