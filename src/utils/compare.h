#pragma once

#include <type_traits>

namespace simd {

	namespace utils {

	    template<std::size_t, std::size_t, typename = void>
	    struct is_less_than : std::false_type 
	    {};

	    template<std::size_t A, std::size_t B>
	    struct is_less_than<A, B, typename std::enable_if<(A < B)>> : std::true_type 
	    {};
	    
    }

}