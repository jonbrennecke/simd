#pragma once

#include "noop.h"
#include <utility>

namespace simd {

    namespace utils {

        namespace impl {

        	template <typename T, typename C, std::size_t ... I>
	        constexpr inline C __apply_constant(T && t, std::index_sequence<I...> ) {
	            return {{ noop<I>(t)... }};
	        }

        }

        template <typename T, std::size_t N, typename C>
	    constexpr inline C apply_constant(T && t) {
	        return impl::__apply_constant<T, C>(std::forward<T>(t), std::make_index_sequence<N>{});
	    }
    }
    
}