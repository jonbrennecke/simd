#pragma once

#include "noop.h"
#include <utility>

namespace simd {

    namespace utils {

        namespace impl {

            template <typename F, typename C, std::size_t ... I>
            constexpr inline C __applyn(F && fn, std::index_sequence<I...> ) {
                return {{ noop<I>(fn())... }};
            }

        }

        template <typename C, std::size_t N, typename F>
        constexpr inline decltype(auto) applyn(F && fn) {
            return impl::__applyn<F, C>(std::forward<F>(fn), std::make_index_sequence<N>{});
        }

    }
    
}