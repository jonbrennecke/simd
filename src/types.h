#pragma once

#include <cstdint>
#include <type_traits>
#include <functional>
#include <tuple>
#include <stdalign.h>
#include "polyfill/void.h"

#ifdef _MSCV_VER_

    #include <intrin.h>

    #pragma intrinsic ( _mm_hadd_pd )

    #define ALIGNED(n) __declspec(align((n)))

    #define ALIGNED_MALLOC(ptr,align,size) ptr = _aligned_malloc((size),(align))

#else

    #include <x86intrin.h> // SSE
    #include <immintrin.h> // AVX

    #define ALIGNED(n) __attribute__((aligned ((n))))

    #define ALIGNED_MALLOC(ptr,align,size) posix_memalign((void**)&(ptr),(align), (size))

    #define restrict __restrict__

#endif

namespace simd {

    // ---------------------------- m128

    template<typename, typename = void>
    struct m128; /* undefined */

    template<typename T>
    struct m128<T, typename std::enable_if<std::is_integral<T>::value>::type> {
        typedef __m128i type;
    };

    template<typename T>
    struct m128<T, typename std::enable_if<std::is_same<T, double>::value>::type> {
        typedef __m128d type;
    };

    template<typename T>
    struct m128<T, typename std::enable_if<std::is_same<T, float>::value>::type> {
        typedef __m128 type;
    };

    template<typename T>
    using m128_t = typename m128<T>::type;

    // ---------------------------- m256

    template<typename, typename = void>
    struct m256; /* undefined */

    template<typename T>
    struct m256<T, typename std::enable_if<std::is_integral<T>::value>::type> {
        typedef __m256i type;
    };

    template<typename T>
    struct m256<T, typename std::enable_if<std::is_same<T, double>::value>::type> {
        typedef __m256d type;
    };

    template<typename T>
    struct m256<T, typename std::enable_if<std::is_same<T, float>::value>::type> {
        typedef __m256 type;
    };

    template<typename T>
    using m256_t = typename m256<T>::type;

    // ---------------------------------- simd_t

    template<typename, std::size_t>
    struct __simd; /* undefined */

    template<typename T>
    struct __simd<T, 256> {
        typedef m256_t<T> type;
    };

    template<typename T>
    struct __simd<T, 128> {
        typedef m128_t<T> type;
    };

    template<typename T, std::size_t S>
    using simd_t = typename __simd<T, S>::type;

    // ---------------------------------- test_simd_size

    template<typename, std::size_t, typename = void_t<> >
    struct test_simd_size : std::false_type {};

    template<typename T, std::size_t S>
    struct test_simd_size<T, S, void_t<simd_t<T, S * sizeof(T) * 8> > > : std::true_type {};

    // ---------------------------------- simd_t_for

    template<typename T, std::size_t S>
    using simd_t_for = simd_t<T, S * sizeof(T) * 8>;

    // ---------------------------------- simd_traits

    template <typename T, std::size_t S>
    struct simd_traits; /* undefined */

    // double

    template <>
    struct simd_traits<double, 2> {
        typedef std::tuple<double, double> tuple_type;
        constexpr static std::size_t size = 2;
    };

    template <>
    struct simd_traits<double, 4> {
        typedef std::tuple<double, double, double, double> tuple_type;
        constexpr static std::size_t size = 4;
    };

    // float

    template <>
    struct simd_traits<float, 4> {
        typedef std::tuple<float, float, float, float> tuple_type;
        constexpr static std::size_t size = 4;
    };

    template <>
    struct simd_traits<float, 8> {
        typedef std::tuple<float, float, float, float, float, float, float, float> tuple_type;
        constexpr static std::size_t size = 8;
    };

    // int64_t

    template <>
    struct simd_traits<int64_t, 2> {
        typedef std::tuple<int64_t, int64_t> tuple_type;
        constexpr static std::size_t size = 2;
    };

    template <>
    struct simd_traits<int64_t, 4> {
        typedef std::tuple<int64_t, int64_t, int64_t, int64_t> tuple_type;
        constexpr static std::size_t size = 4;
    };

    // int32_t

    template <>
    struct simd_traits<int32_t, 4> {
        typedef std::tuple<int32_t, int32_t, int32_t, int32_t> tuple_type;
        constexpr static std::size_t size = 4;
    };

    template <>
    struct simd_traits<int32_t, 8> {
        typedef std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t> tuple_type;
        constexpr static std::size_t size = 8;
    };

    // int16_t

    template <>
    struct simd_traits<int16_t, 8> {
        typedef std::tuple<int16_t, int16_t, int16_t, int16_t, int16_t, int16_t, int16_t, int16_t> tuple_type;
        constexpr static std::size_t size = 8;
    };

    template <>
    struct simd_traits<int16_t, 16> {
        typedef std::tuple<
            int16_t, int16_t, int16_t, int16_t, int16_t, int16_t, int16_t, int16_t,
            int16_t, int16_t, int16_t, int16_t, int16_t, int16_t, int16_t, int16_t
        > tuple_type;
        constexpr static std::size_t size = 16;
    };

    // int8_t

    template <>
    struct simd_traits<int8_t, 16> {
        typedef std::tuple<
            int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t,
            int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t
        > tuple_type;
        constexpr static std::size_t size = 16;
    };

    template <>
    struct simd_traits<int8_t, 32> {
        typedef std::tuple<
            int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t,
            int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t,
            int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t,
            int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t, int8_t
        > tuple_type;
        constexpr static std::size_t size = 32;
    };
}
