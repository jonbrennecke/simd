#pragma once

namespace simd {

    namespace impl {

        template <std::size_t ... I>
        constexpr inline decltype(auto) set_epi64(typename simd_traits<int64_t, 2>::tuple_type && t, std::index_sequence<I...>)
        {
            return _mm_set_epi64x(std::get<sizeof...(I) - 1 - I>(std::forward<decltype(t)>(t))...);
        }

        template <std::size_t ... I>
        constexpr inline decltype(auto) set_epi64(typename simd_traits<int64_t, 4>::tuple_type && t, std::index_sequence<I...>)
        {
            return _mm256_set_epi64x(std::get<sizeof...(I) - 1 - I>(std::forward<decltype(t)>(t))...);
        }

        template <std::size_t ... I>
        constexpr inline decltype(auto) set_pd(typename simd_traits<double, 4>::tuple_type && t, std::index_sequence<I...>)
        {
            return _mm256_set_pd(std::get<sizeof...(I) - 1 - I>(std::forward<decltype(t)>(t))...);
        }

        template <std::size_t ... I>
        constexpr inline decltype(auto) set_epi32(typename simd_traits<int32_t, 4>::tuple_type && t, std::index_sequence<I...>)
        {
            return _mm_set_epi32(std::get<sizeof...(I) - 1 - I>(std::forward<decltype(t)>(t))...);
        }

        // ---------------------------------- set_forwarder

        template <typename T, std::size_t S>
        constexpr inline decltype(auto) set_forwarder(typename simd_traits<T, S>::tuple_type && t);

        template <>
        constexpr inline decltype(auto) set_forwarder<int64_t, 2>(typename simd_traits<int64_t, 2>::tuple_type && t)
        {
            return set_epi64(std::forward<decltype(t)>(t), std::make_index_sequence<2>{});
        }

        template <>
        constexpr inline decltype(auto) set_forwarder<int64_t, 4>(typename simd_traits<int64_t, 4>::tuple_type && t)
        {
            return set_epi64(std::forward<decltype(t)>(t), std::make_index_sequence<4>{});
        }

        template <>
        constexpr inline decltype(auto) set_forwarder<double, 4>(typename simd_traits<double, 4>::tuple_type && t)
        {
            return set_pd(std::forward<decltype(t)>(t), std::make_index_sequence<4>{});
        }

        template <>
        constexpr inline decltype(auto) set_forwarder<int32_t, 4>(typename simd_traits<int32_t, 4>::tuple_type && t)
        {
            return set_epi32(std::forward<decltype(t)>(t), std::make_index_sequence<4>{});
        }

    }

    template <typename T, std::size_t S, typename ... Args >
    constexpr inline decltype(auto) set(Args && ... args) {
        return impl::set_forwarder<T, S>(std::forward_as_tuple(args...));
    }
}