NAME = simd_test
SRC = ./src
TEST = ./test
INC = ./include
BUILD = ./target/debug
SYS_INC = /usr/local/include
SYS_LIB = /usr/local/lib
FRAMEWORKS = /System/Library/Frameworks

# Boost
BOOST_LIB = $(SYS_LIB)
BOOST_INC = $(SYS_INC)/boost
BOOST = -L$(BOOST_LIB) -I$(BOOST_INC)

CC = g++
CFLAGS = -std=c++14 -Wall
CFLAGS += -O3 -Os -march=native -msse2 -msse4.2
CSRC = $(TEST)/test.cpp
CLIBS = $(BOOST)
CINC = -I$(INC) -I./lib
CBUILD = $(BUILD)/lib$(NAME).so

all: 
	$(CC) $(CFLAGS) $(CLIBS) $(CINC) $(CSRC)

install:
	@cp $(BUILD)/* /usr/local/lib

clean:
	rm -rf $(BUILD)/*